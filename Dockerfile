FROM node:12.13.1

RUN mkdir -p /var/www && \
  apt-get update && \
  apt-get install -y authbind git && \
  rm -rf /var/lib/apt/lists/* && \
  touch /etc/authbind/byport/443 && \
  touch /etc/authbind/byport/80 && \
  chown -R root:root /etc/authbind/byport/ && \
  chmod -R 755 /etc/authbind/byport/ && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /var/www/

EXPOSE 80
EXPOSE 443

# The real problem here is that the linux filesystem that owns the files, when mounting them into the container
# with docker compose mounts the files in as root while the container is running as user: node. The causes an
# issue writing to the volume. We decided to run the container with internal root permissions to that volume for now.
USER root

ENV NODE_ENV production
ENV PATH /var/www/node_modules/.bin:$PATH

COPY ./ /var/www
USER root

RUN cd /var/www; npm install --production

RUN npm rebuild

ENTRYPOINT ["authbind", "--deep"]

CMD forever server.js
