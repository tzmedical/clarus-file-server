# Helm Chart for Clarus File Server Deployment

In order to deploy this deployment to a stack you must do the following things.
* Create a Google Cloud Platform Account
* Install gcloud 
* Install kubectl
* Install Helm
* Create a kubernetes Cluster in GCP
* Connect to the gcloud project
* Connect to the kubernetes cluster
* Create a secrets file by copying the secrets.yaml.dist -> secrets.sec.yaml
* Uncomment the file you just made by removing the # from the beginning of each line
* Update the values with the appropriate values
* Install Tiller on the cluster using helm init
* helm install from the root folder using ``` helm install --name clarus-file-server helm/```

### Prerequisites

What things you need to install the software and how to install them

```
https://cloud.google.com/sdk/docs/quickstart-linux
gcloud <connect to the correct project and kubernetes stack>

gcloud components install kubectl
kubectl <connect to your stack>

sudo snap install helm --classic
helm init <this deploys tiller to your kubernetes stack>

helm install clarus-file-server .
helm upgrade <random name it gives you> . # to deploy changes you have made
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details