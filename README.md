# Clarus File Server

Serves and receives files from TZ Medical Clarus devices

## Setup for Running Locally
* In order to start the system locally you must specify a .env file containing required files.
* Copy .env.dist to .env and fill out the variables.
* The .env file contains enviroment values see following for instructions.
  * NODE_ENV=for development set to development and for production set to production
  * NODE_PORT= the port the container will be exposing, by default the container exposes 31010(This is not the port at which you access the _service_.)
  * ENABLE_CLOUD_STORAGE=true If you have a google project with a bucket and auth credentials for a service account ready to go.
  * ENABLE_CLOUD_STORAGE=false This will save files to the file system. If using this option make sure to have a a directory called files in the root that has read and write access.
  

## Setup for Kubernetes -- Skip this if your not using a kubernetes cloud cluster
* In order to do a kubernetes deployment using helm you must createa  secrets.yaml file.
* Copy clarus-file-server/templates/secrets.yaml.dist to clarus-file-server/templates/secrets.yaml and fill out the values, making sure to base64 encode the values.
* Non secret values required for helm-kubernetes deployments are found in values.yaml.
  * NODE_ENV=production
  * NODE_PORT= the port the container will be exposing, by default the container exposes 31010(This is not the port at which you access the _service_.)
  * ENABLE_CLOUD_STORAGE=true If you have a google project with a bucket and auth credentials for a service account ready to go.
  * ENABLE_CLOUD_STORAGE=false This will save files to the file system. If using this option make sure to have a a directory called files in the root that has read and write access.
* Secrets specified in values.yaml are key-key matches secrets here are mapped to secrets.yaml file so they must have the same values, from here its mapped to the containers runtime in kubernetes.
* Each value in the values.yaml secrets section must be mapped to a value in the secrets.yaml file you created.
The secrets section in the secrets.yaml. If you are adding any of the following for use with kubernetes please base64 encode them and put them in the newly created secrets.yaml file.
  * GOOGLE_PROJECT= The project needed in order to enable either of the features [cloud storage, and the web ui]. Created when a new google project is created.
  * GOOGLE_BUCKET= The name of the bucket you plan to have files stored to in google cloud storage.

* The service section of values.yaml specifies the exposed kubernetes service port. The default value is 1537.

## Deployment

### Using Docker
Prerequisites:

* Server running the Docker daemon
* Can deploy to a stack if familiar with kubernetes deployments. Helm/Tiller charts contained within the repo.

Instructions:

### Serving via GCR Image locally
* Modify the ports section in `production-compose.yml` to change which port the application is server on from the host. The default value is 1537.
* To start a docker-compose server use `docker-compose -f production-compose.yml up`.

### Serving a dockerfile build locally
#### For development testing in docker compose
* Modify the ports section in `dev.yml` to change which port the application is served on from the host. The default value is 1537.
* To start a docker-compose server use `docker-compose -f dev.yml up`.
* GOOGLE_AUTH must be defined set and passed in as an environment variable in your .env file. Since the docker container has its own run time that isn't authenticated with google cloud platform

#### For production testing in docker compose
* Modify the ports section in `docker-compose.yml` to change which port the application is served on from the host. The default value is 1537.
* To start a docker-compose server use `docker-compose -f docker-compose.yml up`.
* GOOGLE_AUTH must be defined set and passed in as an environment variable in your .env file. Since the docker container has its own run time that isn't authenticated with google cloud platform

### Using native node.js
Prerequisites:

* Server with node (> 12.13.1) installed

Instructions:

* Gcloud must be authenticated locally or GOOGLE_AUTH must be defined set and passed in as an environment variable in your .env file. Since the docker container has its own run time that isn't authenticated with google cloud platform
* `yarn install -g forever`
* `yarn install`
* To run unit tests locally without dockerization use the following command `npm test`. After you set up the necessary environment variables in a file called .env see .env.dist for requirements.
* To start development run `npm dev`
* To start the server run `npm start`