/* eslint-env node */
/*eslint no-loop-func: 0*/
/*eslint no-process-env: 0*/

// Bootstrap environment variables
require('dotenv').config();

function exposeEnviromentVariables() {
  if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = "development";
  }

  if (!process.env.NODE_PORT) {
    process.env.NODE_PORT = 3000;
  }

  if (process.env.ENABLE_CLOUD_STORAGE === undefined && process.env.NODE_ENV === 'development') {
    process.env.ENABLE_CLOUD_STORAGE = true;
  } else if (process.env.ENABLE_CLOUD_STORAGE === undefined) {
    process.env.ENABLE_CLOUD_STORAGE = false;
  }
}

function getAllMethods(obj) {
  let props = [];

  do {
    props = props.concat(Object.getOwnPropertyNames(obj));
  } while (obj = Object.getPrototypeOf(obj));
  return props;
}

module.exports = {exposeEnviromentVariables, getAllMethods};