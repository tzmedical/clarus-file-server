/* eslint-disable func-names */
/* eslint-disable max-nested-callbacks */
/* eslint-disable no-sync */
/* eslint-env node, mocha */
/*eslint max-nested-callbacks: ["error", 4]*/

"use strict";

const chai = require("chai");
const fs = require("fs");
const chaiHttp = require("chai-http");
const sinon = require("sinon");

const expect = chai.expect;

const app = require("../app");
const util = require("../app/util");
const config = require("../app/config");
const stubSandbox = sinon.sandbox.create();

chai.config.includeStack = true;
chai.use(chaiHttp);

describe("Route testing", function() {
  this.timeout(10 * 1000);

  const testingDeviceSerial = '123456';
  const testFilesBasePath = './test/files';

  const testFileTZA = fs.readFileSync(`${testFilesBasePath}/test.tza`);
  const testFileTZE = fs.readFileSync(`${testFilesBasePath}/test.tze`);
  const testFileSCP = fs.readFileSync(`${testFilesBasePath}/test.scp`);
  const testFileTZR = fs.readFileSync(`${testFilesBasePath}/test.tzr`);
  const testFileTZS = fs.readFileSync(`${testFilesBasePath}/test.tzs`);


  if (config.features.storage) {
    before(() => {
      return util
        // Seed test file tza files
        .writeFile(`${config.env}/files/test.tza`, testFileTZA)
        // Seed test file tzs files
        .then(() => util.writeFile(`${config.env}/files/test.tzs`, testFileTZS))
        // Seed test file tze files
        .then(() => util.writeFile(`${config.env}/files/test.tze`, testFileTZE))
        // Seed test file tzr files
        .then(() => util.writeFile(`${config.env}/files/test.tzr`, testFileTZR))
        // Seed test file scp files
        .then(() => util.writeFile(`${config.env}/files/test.scp`, testFileSCP))

        // Seed test file tza for specific test device requests
        .then(() => util.writeFile(`${config.env}/files/actions/${testingDeviceSerial}/${testingDeviceSerial}.tza`, testFileTZA))

        // Seed test file tzs for specific test device requests
        .then(() => util.writeFile(`${config.env}/files/settings/${testingDeviceSerial}/${testingDeviceSerial}.tzs`, testFileTZS));
    });
  }

  afterEach(() => {
    stubSandbox.restore();
  });

  it("should accept GET /actions and respond with the file", done => {
    const tzaFileName = 'test.tza';
    let tzaFileLocation;
    // if cloud storage get from cloud storage
    let actionExpectedToBeSent;

    const binaryParser = (res, cb) => {
      res.setEncoding("binary");
      res.data = "";
      res.on("data", chunk => {
        res.data += chunk;
      });
      res.on("end", () => {
        cb(null, new Buffer(res.data, "binary"));
      });
    };

    if (config.features.storage) {
      tzaFileLocation = `${config.env}/files/${tzaFileName}`;
    } else {
      tzaFileLocation = `test/files/${tzaFileName}`;
    }

    util
      .readFile(tzaFileLocation)
      .then(data => {
        actionExpectedToBeSent = data;

        if (!config.features.storage) {
          stubSandbox.stub(fs, "readFile").callsFake((path, callback) => {
            callback(null, actionExpectedToBeSent);
          });
        }
      })
      .then(() => {
        chai
          .request(app)
          .get(`/actions/123456`)
          .buffer()
          .parse(binaryParser)
          .end((err, res) => {
            if (err) {
              done(err);
            }

            expect(actionExpectedToBeSent).to.deep.equal(res.body);
            done();
          });
      });
  });

  it("should accept GET /settings and respond with the file", done => {
    const tzsFileName = 'test.tzs';
    let tzsFileLocation;
    let settingsExpectedToBeSent;

    const binaryParser = (res, cb) => {
      res.setEncoding("binary");
      res.data = "";
      res.on("data", chunk => {
        res.data += chunk;
      });
      res.on("end", () => {
        cb(null, new Buffer(res.data, "binary"));
      });
    };

    if (config.features.storage) {
      tzsFileLocation = `${config.env}/files/${tzsFileName}`;
    } else {
      tzsFileLocation = `test/files/${tzsFileName}`;
    }

    util
      .readFile(tzsFileLocation)
      .then(data => {
        settingsExpectedToBeSent = data;

        if (!config.features.storage) {
          stubSandbox.stub(fs, "readFile").callsFake((path, callback) => {
            callback(null, settingsExpectedToBeSent);
          });
        }
      })
      .then(() => {
        chai
          .request(app)
          .get(`/settings/123456`)
          .buffer()
          .parse(binaryParser)
          .end((err, res) => {
            if (err) {
              done(err);
              return;
            }

            expect(settingsExpectedToBeSent).to.deep.equal(res.body);
            done();
          });
      });
  });

  it("should accept PUT /events and store the file", done => {
    const tzeFileName = 'test.tze';
    const tzaFileName = 'test.tza';
    let tzeFileLocation, tzaFileLocation;
    let tzeFileContents, tzaFileContents, proccessedTZEFileLocation;

    if (config.features.storage) {
      tzeFileLocation = `${config.env}/files/${tzeFileName}`;
      tzaFileLocation = `${config.env}/files/${tzaFileName}`;
      proccessedTZEFileLocation = `${config.env}/files/events/${testingDeviceSerial}/${tzeFileName}`;
    } else {
      tzeFileLocation = `test/files/${tzeFileName}`;
      tzaFileLocation = `test/files/${tzaFileName}`;
      proccessedTZEFileLocation = `files/events/${testingDeviceSerial}/${tzeFileName}`;
    }

    const binaryParser = (res, cb) => {
      res.setEncoding("binary");
      res.data = "";
      res.on("data", chunk => {
        res.data += chunk;
      });
      res.on("end", () => {
        cb(null, new Buffer(res.data, "binary"));
      });
    };

    util
      .readFile(tzaFileLocation)
      .then(data => {
        tzaFileContents = data;

        if (!config.features.storage) {
          stubSandbox.stub(fs, "readFile").callsFake((path, callback) => {
            callback(null, tzaFileContents);
          });
        }
      })
      .then(() => util.readFile(tzeFileLocation))
      .then(data => {
        tzeFileContents = data;
      })
      .then(() => {
        chai
          .request(app)
          .put(`/events/123456/${tzeFileName}`)
          .buffer()
          .parse(binaryParser)
          .send(tzeFileContents)
          .end((err, res) => {
            if (err) {
              done(err);
              return;
            }

            util
              .readFile(proccessedTZEFileLocation)
              .then(processedTZEFileContents => {
                expect(processedTZEFileContents).to.deep.equal(tzeFileContents);
                expect(res.body).to.deep.equal(tzaFileContents);
                done();
              })
              .catch(err => {
                throw err;
              });
          });
      })
      .catch(err => done(err));
  });

  it("should accept PUT /ecgs and store the file", done => {
    const scpFileName = 'test.scp';
    const tzaFileName = 'test.tza';
    let scpFileLocation, tzaFileLocation;
    let scpFileContents, tzaFileContents, proccessedSCPFileLocation;

    if (config.features.storage) {
      scpFileLocation = `${config.env}/files/${scpFileName}`;
      tzaFileLocation = `${config.env}/files/${tzaFileName}`;
      proccessedSCPFileLocation = `${config.env}/files/ecgs/${testingDeviceSerial}/${scpFileName}`;
    } else {
      scpFileLocation = `test/files/${scpFileName}`;
      tzaFileLocation = `test/files/${tzaFileName}`;
      proccessedSCPFileLocation = `files/ecgs/${testingDeviceSerial}/${scpFileName}`;
    }

    const binaryParser = (res, cb) => {
      res.setEncoding("binary");
      res.data = "";
      res.on("data", chunk => {
        res.data += chunk;
      });
      res.on("end", () => {
        cb(null, new Buffer(res.data, "binary"));
      });
    };

    util
      .readFile(tzaFileLocation)
      .then(data => {
        tzaFileContents = data;

        if (!config.features.storage) {
          stubSandbox.stub(fs, "readFile").callsFake((path, callback) => {
            callback(null, tzaFileContents);
          });
        }
      })
      .then(() => util.readFile(scpFileLocation))
      .then(data => {
        scpFileContents = data;
      })
      .then(() => {
        chai
          .request(app)
          .put(`/ecgs/123456/${scpFileName}`)
          .buffer()
          .parse(binaryParser)
          .send(scpFileContents)
          .end((err, res) => {
            if (err) {
              done(err);
              return;
            }

            util
              .readFile(proccessedSCPFileLocation)
              .then(processedTZEFileContents => {
                expect(processedTZEFileContents).to.deep.equal(scpFileContents);
                expect(res.body).to.deep.equal(tzaFileContents);
                done();
              })
              .catch(err => {
                throw err;
              });
          });
      })
      .catch(err => done(err));
  });

  it("should accept PUT /reports and store the file", done => {
    const tzrFileName = 'test.tzr';
    const tzaFileName = 'test.tza';
    let tzrFileLocation, tzaFileLocation;
    let tzrFileContents, tzaFileContents, proccessedTZRFileLocation;

    if (config.features.storage) {
      tzrFileLocation = `${config.env}/files/${tzrFileName}`;
      tzaFileLocation = `${config.env}/files/${tzaFileName}`;
      proccessedTZRFileLocation = `${config.env}/files/reports/${testingDeviceSerial}/${tzrFileName}`;
    } else {
      tzrFileLocation = `test/files/${tzrFileName}`;
      tzaFileLocation = `test/files/${tzaFileName}`;
      proccessedTZRFileLocation = `files/reports/${testingDeviceSerial}/${tzrFileName}`;
    }

    const binaryParser = (res, cb) => {
      res.setEncoding("binary");
      res.data = "";
      res.on("data", chunk => {
        res.data += chunk;
      });
      res.on("end", () => {
        cb(null, new Buffer(res.data, "binary"));
      });
    };

    util
      .readFile(tzaFileLocation)
      .then(data => {
        tzaFileContents = data;

        if (!config.features.storage) {
          stubSandbox.stub(fs, "readFile").callsFake((path, callback) => {
            callback(null, tzaFileContents);
          });
        }
      })
      .then(() => util.readFile(tzrFileLocation))
      .then(data => {
        tzrFileContents = data;
      })
      .then(() => {
        chai
          .request(app)
          .put(`/reports/123456/${tzrFileName}`)
          .buffer()
          .parse(binaryParser)
          .send(tzrFileContents)
          .end((err, res) => {
            if (err) {
              done(err);
              return;
            }

            util
              .readFile(proccessedTZRFileLocation)
              .then(processedTZEFileContents => {
                expect(processedTZEFileContents).to.deep.equal(tzrFileContents);
                expect(res.body).to.deep.equal(tzaFileContents);
                done();
              })
              .catch(err => {
                throw err;
              });
          });
      })
      .catch(err => done(err));
  });

  it("should accept GET / and 200 OK", done => {
    chai
      .request(app)
      .get(`/`)
      .end((err, res) => {
        if (err) {
          done(err);
        }

        expect(res).to.have.status(200);
        done();
      });
  });
});