/* eslint-env node */
"use strict";

// External includes
const path = require('path');
const bodyParser = require('body-parser');

// App includes
const util = require('./util');
const config = require("./config");

const readPath = config.readPath;
const writePath = config.writePath;

module.exports = app => {
// Save the PUT files
  app.put('/:type(ecgs|events|reports)/:serial/:fileName', bodyParser.raw({limit: '10mb'}), (req, res, next) => {
    // Get file name and path from url params
    const serial = req.params.serial;
    const fileName = req.params.fileName;
    const dir = path.join(writePath, req.params.type, serial);
    const fileStorageLocation = `${dir}/${fileName}`;

    // Write file to disk
    util
      .writeFile(fileStorageLocation, req.body)
      .then(() => {
        const actionFileName = path.join(readPath, 'actions', req.params.serial, req.params.serial + ".tza");
        return util.readFile(actionFileName);
      })
      .then(data => res.send(data))
      .catch(err => next(err));
  });

  app.get('/settings/:serial', (req, res, next) => {
    const fileName = path.join(readPath, 'settings', req.params.serial, req.params.serial + ".tzs");
    util
      .readFile(fileName)
      .then(data => {
        res.send(data);
      })
      .catch(err => next(err));
  });

  app.get('/actions/:serial', (req, res, next) => {
    const fileName = path.join(readPath, 'actions', req.params.serial, req.params.serial + ".tza");
    util
      .readFile(fileName)
      .then(data => {
        res.send(data);
      })
      .catch(err => next(err));
  });

  app.get('/', function notFound(req, res, next) {
    res.sendStatus(200);
  });

  app.all('*', function notFound(req, res, next) {
    res.sendStatus(404);
  });
};