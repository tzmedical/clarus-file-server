/* eslint-env node */

// Imports the Google Cloud client library
const Storage = require("@google-cloud/storage");

const config = require("./config");

module.exports = new class CloudStorage {
  constructor() {
    const cloudStorageConfig = {
      projectId: config.google.projectId
    };

    // Check to see if the auth key is set, if so use it.
    if (config.google.auth) {
      cloudStorageConfig.credentials = config.google.auth;
    }

    // Creates a client
    this.storage = new Storage(cloudStorageConfig);
  }

  getFile(fileName) {
    return new Promise((resolve, reject) => {
      this.storage
        .bucket(config.google.storage.bucket)
        .file(fileName)
        .download()
        .then(file => {
          resolve(file[0]);
        }).catch(reject);
    });
  }

  getFilePaths(prefix, delimiter) {
    const options = {};
    if (prefix) {
      options.prefix = prefix;
    }
    if (delimiter) {
      options.delimiter = delimiter;
    }
    return new Promise((resolve, reject) => {
      this.storage
        .bucket(config.google.storage.bucket)
        .getFiles(options)
        .then(results => {
          const fileObjects = results[0];
          const fileNames = fileObjects.map(fileObject => fileObject.name);
          resolve(fileNames);
        })
        .catch(err => reject(err));
    });
  }

  save(fileName, buffer) {
    return new Promise((resolve, reject) => {
      this.storage
        .bucket(config.google.storage.bucket)
        .file(fileName)
        .save(buffer)
        .then(() => {
          resolve("success");
        })
        .catch(err => reject(err));
    });
  }

  createBucket(bucketName) {
    // Creates the new bucket
    return new Promise((resolve, reject) => {
      this.storage
        .createBucket(bucketName)
        .then(() => {
          resolve('Success');
        })
        .catch(err => reject(err));
    });
  }

  deleteFiles(folderName) {
    return new Promise((resolve, reject) => {
      return this.storage
        .bucket(config.google.storage.bucket)
        .deleteFiles({
          prefix: `${folderName}/`
        })
        .then(() => {
          resolve("success");
        })
        .catch(err => reject(err));
    });
  }
}();
