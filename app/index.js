/* eslint-env node */
"use strict";

// External Dependencies
const express = require('express');
const bodyParser = require('body-parser');

// Application Dependencies
const initRoutes = require('./routes');

const app = express();

// Parse raw body as a Buffer
app.use(bodyParser.raw({limit: "10mb", type: function type() {
  return true;
}}));

// Add routes from routes file
initRoutes(app);

// TODO Handle errors better maybe check the request agent if its a browser return html, if its postman return a json object, if its a device ask Chris what to return.
app.use("*", (err, req, res, next) => {
  if (err) {
    const error = new Error(err.message);
    error.statusCode = 400;
    return next(error);
  }
  return next();
});

// Exported for testing
module.exports = app;