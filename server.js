/* eslint-env node */
"use strict";

// Bootstrap environment variables
require('dotenv').config();

const app = require('./app');
const config = require("./app/config");

// Start serrver on the port variable
app.listen(config.port, function onServerStarted() {
  console.log("Server listening on port " + config.port);
});